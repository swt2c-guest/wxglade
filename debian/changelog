wxglade (0.8.3-1) unstable; urgency=medium

  * new upstream version

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 21 Jul 2018 19:34:34 +0200

wxglade (0.8.0-2) unstable; urgency=medium

  * changed the file debian/docs to comply with "dpkg-buildpackage -A".
    Closes: #903344
  * upgraded Standards-Version to 4.1.5

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 09 Jul 2018 10:26:56 +0200

wxglade (0.8.0-1) unstable; urgency=medium

  * New upstream release. Closes: #894453
  * added vcs stuff in debian/control

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 01 Apr 2018 20:59:33 +0200

wxglade (0.8.0~a8-1) unstable; urgency=medium

  * modified slightly /usr/bin/wxglade, which Closes: #853000
  * upgraded to the upstream version 080a8, which Closes: #853009
    and Closes: #875554
  * upgraded Standards-Version to 4.1.1, debhelper level to 10

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 09 Nov 2017 14:47:31 +0100

wxglade (0.7.2-2) unstable; urgency=medium

  * included Carsten Grohman's patch. Closes: #823050

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 03 May 2016 08:32:17 +0200

wxglade (0.7.2-1) unstable; urgency=medium

  * upgraded to the new upstream release; Closes: #802313

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 23 Apr 2016 18:12:28 +0200

wxglade (0.7.1-2) unstable; urgency=medium

  * added Copyright (c) 2011-2016 Carsten Grohmann <mail@carsten-grohmann.de>
    in d/copyright
  * rewritten d/copyright to comply with the new format 1.0
  * added a notice about Perl and Lisp code generation in d/control

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 24 Mar 2016 09:59:46 +0100

wxglade (0.7.1-1) unstable; urgency=medium

  * upgraded to the newest upstream version
  * removed build-dependencies: docbook, docbook-xsl, xsltproc, xmltex,
    xmlto, dblatex, since the manpage is not provided from upstream.
  * simplified a lot the packaging system: files Makefile and setup.py
    are no longer used
  * changed the name of the biary package python-wxglade => wxglade as
    it is built as a private python module
  * changed the section name to devel
  * Closes: 803017; made a manual test, which worked as expected.
  * Closes: #803045; made a manual test, which worked as expected.
  * Closes: #803138; made a manual test, which worked as expected.
  * Closes: #801968; made a manual test, which worked as expected.
  * Closes: #803698; this version gets rid of Egg files, and the version
    number is fixed by debian/rules

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 28 Jan 2016 21:58:42 +0100

wxglade (0.7.1~rc1-3) unstable; urgency=medium

  * removed eggs' files which are harmful for a debian system.
    Closes: #801901

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 22 Oct 2015 23:12:13 +0200

wxglade (0.7.1~rc1-2) unstable; urgency=medium

  * added python-setuptools as a build-dependency, and removed the requirement
    for setuptools-hg in setup.py. Closes: #802115

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 21 Oct 2015 19:45:58 +0200

wxglade (0.7.1~rc1-1) unstable; urgency=medium

  * upgraded to the new upstream version. Closes: #801570
  * adjusted patches, and packaging scripts

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 12 Oct 2015 19:33:07 +0200

wxglade (0.7.0-2) unstable; urgency=medium

  * Included Carsten Grohman's patches, sent by e-mail.

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 28 Oct 2014 15:01:41 +0100

wxglade (0.7.0-1) unstable; urgency=medium

  * upgraded to the new upstream release
  * inhibited dh_auto_test to prevent a failure due to fakeroot
  * removed the tests/ directory from the binary package

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 26 Oct 2014 21:34:08 +0100

wxglade (0.6.8-3) unstable; urgency=medium

  * upgraded Standards-Version to 3.9.6
  * simplified d/rules, created d/python-wxglade.install
  * modified common.py assigned icons_path to '/usr/share/wxglade/icons'
    Closes: #766743
  * modified setup.py, created d/python-wxglade.dirs to simplify d/rules
    further.
  * changed debian/patches to avoid multiple modifications of wxglade.py

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 25 Oct 2014 17:32:19 +0200

wxglade (0.6.8-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update for wxPython3.0:
    + debian/control: Update dependency to python-wxgtk3.0 (Closes: #758941)
    + New patch: 60-wxpython3.0.patch

 -- Olly Betts <olly@survex.com>  Mon, 13 Oct 2014 15:55:13 +1300

wxglade (0.6.8-2) unstable; urgency=medium

  * applied Carsten Grohman's patch. Closes: #758254; Closes: #758941
  * upgraded Standards-Version to 3.9.5
  * moved images files from usr/lib/python2.7/dist-packages/wxglade/icons/ to
    usr/share/wxglade/icons

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 18 Sep 2014 14:22:55 +0200

wxglade (0.6.8-1) unstable; urgency=low

  * upgraded to the newest upstream version
  * removed two obsoleted patches

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 26 Jul 2013 23:04:59 +0200

wxglade (0.6.7-2) unstable; urgency=low

  * fixed a set of ugly bugs, thanks to Carsten Grohmann

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 25 May 2013 23:32:20 +0200

wxglade (0.6.7-1) unstable; urgency=low

  * upgraded to the newest release
  * changed my DebEmail
  * updated Standards-Version, compat, debhelper build-dependency

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 23 Apr 2013 17:35:44 +0200

wxglade (0.6.5-2) unstable; urgency=low

  * added a build-dependency on python-wxgtk2.8 which is necessary to
    run tests during the package build. Closes: #663567
  * bumped Standards-Version to 3.9.3

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 11 Apr 2012 17:19:20 +0000

wxglade (0.6.5-1) unstable; urgency=low

  * upgraded to the newest version
  * removed some debian patches, then made patches for the launcher and
    for the location of the help file, and legal stuff.
  * rewritten pat of debian/copyright to update it.

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 16 Jan 2012 01:13:07 +0100

wxglade (0.6.4-2) unstable; urgency=low

  * restored the menu file and the icon. Closes: #653389
  * restored the desktop file.
  * added --with python2 as an option for dh $@

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 27 Dec 2011 18:52:50 +0100

wxglade (0.6.4-1) unstable; urgency=low

  * upgraded to the new upstream version

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 22 Dec 2011 19:01:36 +0100

wxglade (0.6.3+rel-5) unstable; urgency=low

  * removed the alternative dependency on python-wxgtk2.6. Closes:#647734
  * upgraded Standards-Version

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 05 Nov 2011 19:31:34 +0100

wxglade (0.6.3+rel-4) unstable; urgency=low

  * added a desktop file for Gnome. Closes: #594137

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 24 Aug 2010 02:23:34 +0200

wxglade (0.6.3+rel-3) unstable; urgency=low

  * added a file debian/menu and a pixmap to have an entry in KDE/Debian 
    menus. Closes: #588359

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 15 Aug 2010 15:52:58 +0200

wxglade (0.6.3+rel-2) unstable; urgency=low

  * upgraded Standards-Version to 3.9.1
  * added a dependency on docbook-xsl dblatex and xmlto
  * modified the Makefile to use docbook-xsl's files. Closes: #590423

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 08 Aug 2010 18:16:50 +0200

wxglade (0.6.3+rel-1) unstable; urgency=low

  * got the newest version from upstream, which had changed a little
  * repackaged wxglade with the new quilt-based system
  * created a file setup.py to build the application
  * changed the charset=CHARSET stance in de.po and fr.po to use UTF-8 instead
  * made wx-glade useable with python 2.6, by declaring "XS-Python-Version: all"
    Closes: #587692
  * fixed a typo in main.py which prevented the access to documentations.

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 03 Jul 2010 03:30:30 +0200

wxglade (0.6.3-2) unstable; urgency=low

  * upgraded lintian and fixed the newly reported errors: 
    standards-version, etc.
  * modified the path to html documents in main.py
    Closes: #533547
  * added a dependency over xmltex, and some lines to generate the PDF 
    documents.
    Closes: #552784
  * the dependency upon python-wxgtk is now 
    python-wxgtk2.8|python-wxgtk2.6
    Closes: #501710
  * The crash of wxglade when creating a wxFrame does no more 
    occur with the version 0.6.3-2 of this package (bug not 
    reproducible)
    Closes: #537452

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 29 Oct 2009 16:06:10 +0100

wxglade (0.6.3-1) unstable; urgency=low

  * upgraded standards-version (now 3.8.0)
  * added an invokation of dh_desktop from debian/rules
  * as the build system should correctly manage the man page, thus
    Closes: #475661

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 13 Jul 2008 21:11:32 +0200

wxglade (0.6.3-0.1) unstable; urgency=low

  * NMU
  * New upstream version. Closes: #468874.
  * Don't hardcode location of the shared python files. Closes: #472037.
  * debian/rules (clean): Remove debian/files, as shipped by upstream.
    Closes: #451061.

 -- Matthias Klose <doko@debian.org>  Sat, 29 Mar 2008 17:28:59 +0100

wxglade (0.6.1-2) unstable; urgency=low

  * Added a .desktop file and two pixmaps
    Closes: #454154.
  * The starter script checks no more the current python version
    Closes: #348871

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 09 Dec 2007 19:54:26 +0100

wxglade (0.6.1-1) unstable; urgency=low

  * upgraded to the new upstream version
  * erased the .mo files in the source tree
  * added the installation of .mo files by the Makefile

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 10 Nov 2007 16:31:54 +0100

wxglade (0.5-1) unstable; urgency=low

  * Upgraded to the new upstream version
    Closes: #422710.

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 08 May 2007 19:48:06 +0200

wxglade (0.4.1-3) unstable; urgency=low

  * cleaned thouroughly Makefile. Increased the compatibility with new 
    policies. Fixed the wrong symlink to the images.
    Closes: #386049.

 -- Georges Khaznadar <georgesk@ofset.org>  Fri, 15 Sep 2006 19:10:10 +0200

wxglade (0.4.1-2) unstable; urgency=low

  * Remove hard-code dependency on a python version in bin/wxglade.
    Closes: #384096.

 -- Matthias Klose <doko@debian.org>  Mon, 28 Aug 2006 23:52:38 +0000

wxglade (0.4.1-1) unstable; urgency=low

  * New upstream version (as released on sf). Closes: #327851, #340999.
  * Convert to updated Python policy. Closes: #373409.

 -- Matthias Klose <doko@debian.org>  Thu, 29 Jun 2006 02:00:06 +0200

wxglade (0.4.1) unstable; urgency=low

  * added a Conflicts: against libwxgtk2.4-python, thanks to  Andrea Brenci
    <andrea.brenci@libero.it>
  * added a menu file, thanks to Basil Shubin <bashu@yandex.ru>
    Closes #335945, #335945

 -- Georges Khaznadar <georgesk@ofset.org>  Fri, 11 Nov 2005 18:37:36 +0100

wxglade (0.4) unstable; urgency=low

  * new upstream version
  * fixed one bug about destroying the .xvpics dirs

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 27 Oct 2005 17:19:56 +0200

wxglade (0.3.5.cvs20050824-0.1) unstable; urgency=low

  * Sponsored upload.
  * renamed the package to python-wxglade. the python-wxgtk packages
    don't have a version schema as well, so it doesn't make sense with
    wxglade as well.
  * Depend on python-wxgtk2.6 | python-wxgtk. These are the new names,
    and you have at least to depend on a real package.
  * Build-depend on python (needed for dh_python).
  * Repackage as a .orig.tar.gz package.

 -- Matthias Klose <doko@debian.org>  Wed, 24 Aug 2005 07:29:15 +0000

wxglade (0.3.5.cvs20050823) unstable; urgency=low

  * upgraded to last CVS version 
  * updated the dependancies
  * fixed some executable flags

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 23 Aug 2005 10:59:39 +0200

wxglade (0.3.5.cvs20041101-1) unstable; urgency=low

  * upgraded to last CVS version

 -- Georges Khaznadar <gekhajofour@netinfo.fr>  Mon,  1 Nov 2004 16:40:31 +0100

wxglade (0.3.4-1) unstable; urgency=low

  * Initial Release.

 -- Georges Khaznadar <gekhajofour@netinfo.fr>  Mon, 25 Oct 2004 14:09:48 +0200

wxglade (0.4.1.cvs20060221) unstable; urgency=low

  * Sponsored upload.
  * Upgraded to last upstream version
  * Updated the version number to Alberto Griggio's announcement.

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 21 Feb 2006 17:10:35 +0100

wxglade (0.3.5.cvs20060131-0.1) unstable; urgency=low

  * Sponsored upload.
  * synchronised to the CVS.
    Closes #340999, #327851
  * no problem with the menu bars upon testing, so probably ...
    Closes #341762
  * added a Conflicts: against libwxgtk2.4-python, thanks to  Andrea Brenci
    <andrea.brenci@libero.it>
  * added a menu file, thanks to Basil Shubin <bashu@yandex.ru>
    Closes #335945, #335946
  * modified /usr/bin/wxglade to use python2.3
   Closes #348871

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 31 Jan 2006 19:59:41 +0100

wxglade (0.3.5.cvs20050824-0.1) unstable; urgency=low

  * Sponsored upload.
  * renamed the package to python-wxglade. the python-wxgtk packages
    don't have a version schema as well, so it doesn't make sense with
    wxglade as well.
  * Depend on python-wxgtk2.6 | python-wxgtk. These are the new names,
    and you have at least to depend on a real package.
  * Build-depend on python (needed for dh_python).
  * Repackage as a .orig.tar.gz package.

 -- Matthias Klose <doko@debian.org>  Wed, 24 Aug 2005 07:29:15 +0000

wxglade (0.3.5.cvs20050823) unstable; urgency=low

  * upgraded to last CVS version 
  * updated the dependancies
  * fixed some executable flags

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 23 Aug 2005 10:59:39 +0200

wxglade (0.3.5.cvs20041101-1) unstable; urgency=low

  * upgraded to last CVS version

 -- Georges Khaznadar <gekhajofour@netinfo.fr>  Mon,  1 Nov 2004 16:40:31 +0100

wxglade (0.3.4-1) unstable; urgency=low

  * Initial Release.

 -- Georges Khaznadar <gekhajofour@netinfo.fr>  Mon, 25 Oct 2004 14:09:48 +0200
